# Django Project

This is a Django project for Test Project for Argus(Edu Platform)
includes
- admin_dashboard 
- user_profile

## Prerequisites

Make sure you have the following installed on your machine:

- Python 3.x
- pip (Python package installer)

## Installation

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/aziz4ik/edu_argos

- cd your-django-project
 - python -m venv venv
 - Windows: venv\Scripts\activate
- Linux: source venv/bin/activate
- pip install -r requirements.txt
- python manage.py migrate
- python manage.py createsuperuser
- python manage.py runserver

## Navigation

1. admin_dashboard
   - /admin_dashboard
2. /profile/1/

***
After creating superuser with django admin enter django admin
and create user wit role student and moderator, superadmin
***

