from django.contrib import admin

from apps.user.models import User, UserCourseWishlist
from django.contrib.auth.hashers import make_password



@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ['id', 'username']

    def save_model(self, request, obj, form, change):
        obj.password = make_password(obj.password)
        super().save_model(request, obj, form, change)


@admin.register(UserCourseWishlist)
class CourseWishListAdmin(admin.ModelAdmin):
    list_display = ['id']
