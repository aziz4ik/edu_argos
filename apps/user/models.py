from datetime import date

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import CASCADE

from apps.core.models.base_model import BaseModel
from apps.courses.models import Course


class User(AbstractUser):
    USER_TYPE_CHOICES = [
        ('super_admin', 'Super Admin'),
        ('moderator', 'Moderator'),
        ('student', 'Student'),
    ]

    assigned_courses = models.ManyToManyField(Course, related_name='students_assigned', blank=True)
    user_type = models.CharField(max_length=20, choices=USER_TYPE_CHOICES)
    avatar = models.ImageField(upload_to='media')
    birth_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.username

    def calculate_age(self):
        if self.birth_date:
            today = date.today()
            age = today.year - self.birth_date.year - (
                        (today.month, today.day) < (self.birth_date.month, self.birth_date.day))
            return age
        return None

    @property
    def birth_date_str(self):
        return self.birth_date.strftime('%Y-%m-%d') if self.birth_date else ''


class UserCourseWishlist(BaseModel):
    course = models.ManyToManyField(Course, blank=True)
    student = models.ForeignKey(User, CASCADE)

    def __str__(self):
        return f'{self.id}'
