from django.urls import path

from apps.user import views

urlpatterns = [
    path('', views.MainWelcomeTemplateView.as_view(), name='welcome'),
]