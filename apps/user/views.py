from django.views.generic import TemplateView


class MainWelcomeTemplateView(TemplateView):
    template_name = 'welcome.html'
