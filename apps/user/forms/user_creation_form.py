from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model
from django import forms


class CustomUserCreationForm(UserCreationForm):
    class Meta:
        model = get_user_model()
        fields = ['first_name', 'last_name', 'username', 'birth_date', 'password1', 'password2', 'avatar']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Set the initial value for the thumbnail field
        if 'instance' in kwargs:
            instance = kwargs['instance']
            self.fields['avatar'].initial = instance.avatar

        # Set the password fields as not required
        self.fields['password1'].required = False
        self.fields['password2'].required = False

    def clean_username(self):
        username = self.cleaned_data['username']
        instance = getattr(self, 'instance', None)

        # Check uniqueness only if the username is different from the current instance
        if instance and instance.username == username:
            return username

        # Perform the default uniqueness check
        try:
            get_user_model().objects.get(username=username)
        except get_user_model().DoesNotExist:
            return username

        raise forms.ValidationError('A user with that username already exists.')

    def save(self, commit=True):
        user = super().save(commit=False)

        # Check if both new password fields are present in the form data
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2:
            # Update the password only if both new password fields are present
            user.set_password(password1)

        if commit:
            user.save()

        return user
