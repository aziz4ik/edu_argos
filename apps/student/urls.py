from django.urls import path

from apps.student import views

urlpatterns = [
    path('profile/<int:pk>/', views.StudentProfileView.as_view(),  name='profile'),
    path('auth_login/', views.LoginTemplateView.as_view(),  name='login_page'),
    # path('profile/<int:pk>/update_username/', views.StudentProfileView.as_view(),  name='update_username'),
    path('change-avatar/', views.change_avatar, name='change_avatar'),
    path('update-password/', views.update_password, name='update_password'),
    path('edit-username/', views.edit_username, name='edit_username'),
    path('authenticate_user/', views.login_view, name='authenticate_user'),
    path('auth_logout/', views.LogoutView.as_view(), name='auth_logout'),
    path('student_all_courses/', views.StudentAllCoursesListView.as_view(), name='student_view_all_courses'),
    path('add_course_to_wishlist/', views.add_course_to_wishlist, name='add_course_to_wishlist'),
    path('remove_course_from_wishlist/', views.delete_course_from_wishlist, name='remove_course_from_wishlist'),
    path('my_wishlist/', views.StudentWishListCourses.as_view(), name='student_wishlist')
]
