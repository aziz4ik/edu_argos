from django.contrib.auth.decorators import login_required
from django.http import HttpResponseBadRequest
from django.urls import reverse
from django.views.generic import DetailView, UpdateView, TemplateView, View, ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import logout
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.contrib import messages
from django.shortcuts import get_object_or_404

from apps.user.models import User, UserCourseWishlist
from apps.courses.models import Course


class StudentProfileView(LoginRequiredMixin, DetailView):
    model = User
    queryset = User.objects.all()
    template_name = 'student_profile.html'
    login_url = '/auth_login'

    def get_object(self, queryset=None):
        # Ensure that only the authenticated user can view their own profile
        if self.request.user.is_authenticated:
            return self.request.user
        else:
            # If not authenticated, return a 404 response
            return get_object_or_404(User, pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Fetch assigned courses for the authenticated user
        assigned_courses = self.request.user.assigned_courses.all()

        # Add assigned courses to the context
        context['assigned_courses'] = assigned_courses

        # Add any additional context data as needed
        return context


@login_required(login_url='/auth_login')
def change_avatar(request):
    if request.method == 'POST':
        # Use request.FILES to get the file data
        avatar_file = request.FILES.get('avatar')

        # Assuming you have a field 'avatar' in your User model to store the avatar
        request.user.avatar = avatar_file
        request.user.save()

        messages.success(request, 'Avatar updated successfully.')

        user = request.user
        profile_url = reverse('profile', args=[user.pk])
        return redirect(profile_url)


@login_required(login_url='auth_login')
def update_password(request):
    if request.method == 'POST':
        current_password = request.POST.get('current_password')
        new_password = request.POST.get('new_password')
        request.user.set_password(new_password)
        request.user.save()
        messages.success(request, 'Password updated successfully.')
        user = request.user
        profile_url = reverse('profile', args=[user.pk])
        return redirect(profile_url)


@login_required(login_url='/auth_login')
def edit_username(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        request.user.username = username
        request.user.save()
        messages.success(request, 'Username updated successfully.')
        user = request.user
        profile_url = reverse('profile', args=[user.pk])
        return redirect(profile_url)


def login_view(request):
    if request.method == 'POST':
        if request.user.is_authenticated:
            return redirect('/admin')
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            # Redirect to the user's profile page.
            if user.user_type == 'student':
                profile_url = reverse('profile', args=[user.pk])
                return redirect(profile_url)
            elif user.user_type in ['super_admin', 'moderator']:
                return redirect('/admin_dashboard')
        else:
            # Return an 'invalid login' error message.
            error_message = 'Invalid login credentials. Please try again.'
            return render(request, 'login.html', {'error_message': error_message})
    else:
        return render(request, 'login.html')


class LoginTemplateView(TemplateView):
    template_name = 'login.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            if request.user.user_type == 'student':
                # Redirect to student profile page
                return redirect('profile', pk=request.user.pk)
            elif request.user.user_type in ['moderator', 'super_admin']:
                # Redirect to admin_dashboard page
                return redirect('admin_dashboard')  # Replace with the actual URL name for the admin dashboard
            else:
                # Default redirect for unknown user types (you can modify this accordingly)
                return redirect('/admin')

        return super().get(request, *args, **kwargs)


class LogoutView(View):
    success_url = '/auth_login'

    def get(self, request, *args, **kwargs):
        logout(request)
        return redirect('/auth_login')  # Redirect to your login page, update the URL name as needed


class StudentAllCoursesListView(LoginRequiredMixin, ListView):
    model = Course
    template_name = 'student_view_all_courses.html'
    context_object_name = 'all_courses'


class StudentWishListCourses(LoginRequiredMixin, ListView):
    model = UserCourseWishlist
    queryset = UserCourseWishlist.objects.all()
    context_object_name = 'wishlist_courses'
    template_name = 'student_wishlist_courses.html'

    def get_queryset(self):
        # Filter the queryset to retrieve only the wishlist for the current user
        return UserCourseWishlist.objects.filter(student=self.request.user)


@login_required(login_url='/auth_login')
def add_course_to_wishlist(request):
    if request.method == 'GET':
        course_id = request.GET.get('course_id')

        # Check if the course_id is valid (exists in the Course model)
        try:
            course = Course.objects.get(pk=course_id)
        except Course.DoesNotExist:
            return HttpResponseBadRequest("Invalid course_id")

        # Check if the user already has a wishlist
        wishlist, created = UserCourseWishlist.objects.get_or_create(student=request.user)

        # Check if the course is already in the user's wishlist
        if wishlist.course.filter(pk=course_id).exists():
            return HttpResponseBadRequest("Course already in wishlist")

        # Add the course to the user's wishlist
        wishlist.course.add(course)

        return redirect('/my_wishlist')
    else:
        return HttpResponseBadRequest("Invalid request method")


@login_required(login_url='/auth_login')
def delete_course_from_wishlist(request):
    if request.method == 'GET':
        course_id = request.GET.get('course_id')

        # Ensure the user is in the wishlist
        user_wishlist = get_object_or_404(UserCourseWishlist, student=request.user)

        # Remove the course from the wishlist
        course = get_object_or_404(Course, id=course_id)
        user_wishlist.course.remove(course)

        return redirect('/my_wishlist')  # Redirect to the wishlist view or any other appropriate URL
    else:
        return redirect('/my_wishlist')
