from django.contrib import admin

from apps.courses.models import Course, CourseUpdateRequest

admin.site.register(Course)


@admin.register(CourseUpdateRequest)
class CourseUpdateRequestAdmin(admin.ModelAdmin):
    list_display = ('id', 'course', 'status')
    list_filter = ('status',)
