from django.db import models
from django.db.models import CASCADE

from apps.core.models.base_model import BaseModel
from apps.core.constants.update_request_statuses import UPDATE_REQUEST_STATUS_CHOICES


class Course(BaseModel):
    title = models.CharField(max_length=255)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    thumbnail = models.ImageField(upload_to='media/courses')

    def __str__(self):
        return self.title


class CourseUpdateRequest(BaseModel):
    course = models.ForeignKey(Course, CASCADE)
    title = models.CharField(max_length=255, null=True, blank=True)
    price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    thumbnail = models.ImageField(upload_to='media/courses', null=True, blank=True)
    status = models.CharField(max_length=55,
                              choices=UPDATE_REQUEST_STATUS_CHOICES,
                              default='awaiting')

    def __str__(self):
        return f'{self.id}'
