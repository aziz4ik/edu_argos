# Generated by Django 5.0.1 on 2024-01-29 14:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0005_alter_courseupdaterequest_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='courseupdaterequest',
            name='status',
            field=models.CharField(choices=[('approved', 'approved'), ('rejected', 'rejected'), ('awaiting', 'awaiting')], default='awaiting', max_length=55),
        ),
    ]
