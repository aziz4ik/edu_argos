from django import forms

from apps.courses.models import Course, CourseUpdateRequest


class CreateCourseForm(forms.ModelForm):
    class Meta:
        model = Course
        fields = ('title', 'price', 'thumbnail')


class UpdateCourseForm(forms.ModelForm):
    class Meta:
        model = Course
        fields = ['title', 'thumbnail', 'price']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Set the initial value for the thumbnail field
        if 'instance' in kwargs:
            instance = kwargs['instance']
            self.fields['thumbnail'].initial = instance.thumbnail


class UpdateCourseRequestForm(forms.ModelForm):
    class Meta:
        model = CourseUpdateRequest
        fields = ('course', 'title', 'price', 'thumbnail')
