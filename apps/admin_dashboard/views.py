from datetime import date

from django.contrib import messages
from django.contrib.auth.decorators import user_passes_test
from django.core.files.base import ContentFile
from django.db.models import Q, Count
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy, reverse
from django.views import View
from django.views.generic import TemplateView, CreateView, ListView, UpdateView, DeleteView, DetailView

from apps.admin_dashboard.forms.create_course import CreateCourseForm, UpdateCourseRequestForm, UpdateCourseForm
from apps.admin_dashboard.permissions import ModeratorAndAdminPermissionMixin, SuperAdminPermissionMixin, is_super_admin
from apps.courses.models import Course, CourseUpdateRequest
from apps.user.forms.user_creation_form import CustomUserCreationForm
from apps.user.models import User


class AdminWelcomeTemplateView(ModeratorAndAdminPermissionMixin, TemplateView):
    template_name = 'welcome_admin.html'
    login_url = '/auth_login'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Retrieve a list of students
        total_courses = Course.objects.count()
        total_students = User.objects.filter(user_type='student').count()
        students_without_assigned_courses = User.objects.filter(user_type='student').annotate(
            total_assigned_courses=Count('assigned_courses')
        ).filter(total_assigned_courses=0).count()

        context['total_courses'] = total_courses
        context['total_students'] = total_students
        context['students_without_assigned_courses'] = students_without_assigned_courses
        return context


class CreateStudentTemplateView(SuperAdminPermissionMixin, TemplateView):
    template_name = 'create_student.html'


class StudentsListView(ModeratorAndAdminPermissionMixin, ListView):
    model = User
    queryset = User.objects.all()
    template_name = 'all_students.html'
    context_object_name = 'all_students'

    def get_queryset(self):
        qs = self.queryset.filter(user_type='student')

        # Get the search term from the URL parameters
        search_term = self.request.GET.get('q')

        # Get the age range from the URL parameters
        from_age = self.request.GET.get('from_age')
        to_age = self.request.GET.get('to_age')

        # If a search term is provided, filter by first name or last name
        if search_term:
            qs = qs.filter(Q(first_name__icontains=search_term) | Q(last_name__icontains=search_term))

        # If age range is provided, filter by age
        if from_age and to_age:
            today = date.today()
            birth_date_upper = today.replace(year=today.year - int(from_age))
            birth_date_lower = today.replace(year=today.year - int(to_age))
            qs = qs.filter(birth_date__lte=birth_date_upper, birth_date__gte=birth_date_lower)

        return qs


class StudentUpdateView(SuperAdminPermissionMixin, UpdateView):
    model = User
    form_class = CustomUserCreationForm
    template_name = 'update_student.html'
    success_url = reverse_lazy('students_list')


class StudentDetailView(ModeratorAndAdminPermissionMixin, DetailView):
    model = User
    queryset = User.objects.all()
    template_name = 'student_detail.html'


class StudentDeleteView(SuperAdminPermissionMixin, DeleteView):
    model = User

    def get_success_url(self):
        return reverse('students_list')

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.delete()
        return redirect(success_url)


class CoursesListView(ModeratorAndAdminPermissionMixin, ListView):
    model = Course
    queryset = Course.objects.all()
    template_name = 'all_courses.html'
    context_object_name = 'all_courses'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Retrieve a list of students
        students_list = User.objects.filter(user_type='student')
        context['students_list'] = students_list
        return context


class CourseDetailView(ModeratorAndAdminPermissionMixin, DetailView):
    model = Course
    queryset = Course.objects.all()
    template_name = 'course_detail.html'


class CreateCoursesView(SuperAdminPermissionMixin, CreateView):
    model = Course
    queryset = Course.objects.all()
    template_name = 'create_course.html'
    form_class = CreateCourseForm
    success_url = reverse_lazy('courses_list')


class UpdateCoursesView(SuperAdminPermissionMixin, UpdateView):
    model = Course
    queryset = Course.objects.all()
    template_name = 'update_course.html'
    form_class = UpdateCourseForm
    success_url = reverse_lazy('courses_list')


class ModeratorUpdateCourse(ModeratorAndAdminPermissionMixin, CreateView):
    model = CourseUpdateRequest
    form_class = UpdateCourseRequestForm
    template_name = 'create_update_course_request.html'
    success_url = reverse_lazy('courses_list')


class CoursesDeleteView(SuperAdminPermissionMixin, DeleteView):
    model = Course

    def get_success_url(self):
        return reverse('courses_list')

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.delete()
        return redirect(success_url)


class CreateStudentView(SuperAdminPermissionMixin, View):
    template_name = 'create_student.html'
    form_class = CustomUserCreationForm

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            user = form.save(commit=False)
            user.user_type = 'student'
            user.save()
            messages.success(request, 'Student created successfully.')
            return redirect('/students')
        return render(request, self.template_name, {'form': form})


class UpdateRequestTemplateView(SuperAdminPermissionMixin, ListView):
    model = CourseUpdateRequest
    queryset = CourseUpdateRequest.objects.all()
    template_name = 'update_requests.html'
    context_object_name = 'all_update_requests'

    def get_queryset(self):
        qs = self.queryset
        status = self.request.GET.get('status')
        if status:
            if status == 'awaiting':
                qs = qs.filter(status='awaiting')
                return qs
            elif status == 'approved':
                qs = qs.filter(status='approved')
                return qs
            elif status == 'rejected':
                qs = qs.filter(status='rejected')
                return qs
            else:
                return qs


@user_passes_test(is_super_admin, login_url='/auth_login')
def approve_update_request_view(request):
    if request.method == 'GET':
        update_request_id = request.GET.get('update_request_id')

        # Get the CourseUpdateRequest object
        update_request = get_object_or_404(CourseUpdateRequest, id=update_request_id)

        # Retrieve the fields from the update request
        updated_title = update_request.title
        updated_price = update_request.price
        updated_thumbnail = update_request.thumbnail

        # Update the Course model with the new values (considering null fields)
        course = update_request.course
        course.title = updated_title if updated_title is not None else course.title
        course.price = updated_price if updated_price is not None else course.price

        # Update the Course thumbnail only if a new one is provided
        if updated_thumbnail:
            course.thumbnail.delete()  # Delete the original thumbnail
            course.thumbnail.save(updated_thumbnail.name, ContentFile(updated_thumbnail.read()))

        # Save the updated Course model
        course.save()

        # Update the status of the update request to 'approved'
        update_request.status = 'approved'
        update_request.save()

        # Redirect the user to a specific URL (adjust the URL as needed)
        return redirect('/update_requests/?status=approved')


@user_passes_test(is_super_admin, login_url='/auth_login')
def assign_course(request):
    course_id = request.POST.get('course_id')
    course = Course.objects.get(id=course_id)
    if request.method == 'POST':
        student_ids = request.POST.getlist('studentList')

        for student_id in student_ids:
            student = get_object_or_404(User, id=student_id)
            student.assigned_courses.add(course)

        return redirect('courses_list')  # Redirect to the original view or a specific URL
    return redirect('courses_list')  # Redirect to the original view or a specific URL
