from django.contrib.auth.mixins import UserPassesTestMixin


class ModeratorAndAdminPermissionMixin(UserPassesTestMixin):
    def test_func(self):
        # Check if the user is authenticated and has the correct user_type
        return self.request.user.is_authenticated and self.request.user.user_type in ['moderator', 'super_admin']


class SuperAdminPermissionMixin(UserPassesTestMixin):
    def test_func(self):
        # Check if the user is authenticated and has the correct user_type
        return self.request.user.is_authenticated and self.request.user.user_type == 'super_admin'


def is_super_admin(user):
    return user.user_type == 'super_admin'
