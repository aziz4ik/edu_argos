from django.urls import path

from apps.admin_dashboard import views

urlpatterns = [
    path('admin_dashboard/', views.AdminWelcomeTemplateView.as_view(), name='admin_dashboard'),
    path('create_student/', views.CreateStudentTemplateView.as_view(), name='create_student'),
    path('create_new_student/', views.CreateStudentView.as_view(), name='create_new_student'),
    path('delete_student/<int:pk>/', views.StudentDeleteView.as_view(), name='delete_student'),
    path('student/<int:pk>/', views.StudentDetailView.as_view(), name='student_detail'),
    path('update_student/<int:pk>/', views.StudentUpdateView.as_view(), name='update_student'),
    path('students/', views.StudentsListView.as_view(), name='students_list'),


    path('courses/', views.CoursesListView.as_view(), name='courses_list'),
    path('course/<int:pk>/', views.CourseDetailView.as_view(), name='course_detail'),
    path('create_course/', views.CreateCoursesView.as_view(), name='create_course'),
    path('update_course/<int:pk>/', views.UpdateCoursesView.as_view(), name='update_course'),
    path('delete_course/<int:pk>/', views.CoursesDeleteView.as_view(), name='delete_course'),


    path('update_requests/', views.UpdateRequestTemplateView.as_view(), name='update_requests'),
    path('create_course_update_request/', views.ModeratorUpdateCourse.as_view(), name='create_course_update_request'),

    path('approve_update_request/', views.approve_update_request_view, name='approve_update_request'),

    path('assign_course/', views.assign_course, name='assign_course')

]
