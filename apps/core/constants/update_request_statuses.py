
APPROVED = 'approved'
REJECTED = 'rejected'
AWAITING = 'awaiting'

UPDATE_REQUEST_STATUS_CHOICES = (
    (APPROVED, 'approved'),
    (REJECTED, 'rejected'),
    (AWAITING, 'awaiting'),
)
